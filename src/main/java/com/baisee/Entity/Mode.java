package com.baisee.Entity;

public class Mode {
	private Integer id;
	private String county;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public Mode(Integer id, String county) {
		super();
		this.id = id;
		this.county = county;
	}
	public Mode() {
		super();
	}
	@Override
	public String toString() {
		return "Mode [id=" + id + ", county=" + county + "]";
	}
	
}	
