package com.baisee.Entity;

import java.io.Serializable;

public class Signature implements Serializable{
	private String openid;
	private String session_key;
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSession_key() {
		return session_key;
	}
	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
	public Signature(String openid, String session_key) {
		super();
		this.openid = openid;
		this.session_key = session_key;
	}
	public Signature() {
		super();
	}
	@Override
	public String toString() {
		return "Signature [openid=" + openid + ", session_key=" + session_key + "]";
	}
	
}
