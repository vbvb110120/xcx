package com.baisee.Entity;

public class Aging {
	private String timestamp;
	private String appid;
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public Aging(String timestamp, String appid) {
		super();
		this.timestamp = timestamp;
		this.appid = appid;
	}
	public Aging() {
		super();
	}
	
}
