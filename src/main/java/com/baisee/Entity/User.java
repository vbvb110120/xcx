package com.baisee.Entity;

public class User {
	private Integer id;
	private String name;
	private Integer orgid;
	private Integer departmentid;
	private String password;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOrgid() {
		return orgid;
	}
	public void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}
	public Integer getDepartmentid() {
		return departmentid;
	}
	public void setDepartmentid(Integer departmentid) {
		this.departmentid = departmentid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public User(Integer id, String name, Integer orgid, Integer departmentid, String password) {
		super();
		this.id = id;
		this.name = name;
		this.orgid = orgid;
		this.departmentid = departmentid;
		this.password = password;
	}
	public User() {
		super();
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", orgid=" + orgid + ", departmentid=" + departmentid
				+ ", password=" + password + "]";
	}
	
}
