package com.baisee.Entity;

public class County {
	private Integer id;
	private String name;
	private String mode_id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMode_id() {
		return mode_id;
	}
	public void setMode_id(String mode_id) {
		this.mode_id = mode_id;
	}
	public County(Integer id, String name, String mode_id) {
		super();
		this.id = id;
		this.name = name;
		this.mode_id = mode_id;
	}
	public County() {
		super();
	}
	@Override
	public String toString() {
		return "County [id=" + id + ", name=" + name + ", mode_id=" + mode_id + "]";
	}
	
	
}
