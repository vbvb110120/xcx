package com.baisee.Entity;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class WxUser {
	private Integer id;
	private String nickname;
	private String gender;
	private String country;
	private String avatarurl;
	private Integer phone;
	private String logintime;
	private String openid;
	private List<Aging> watermark;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAvatarurl() {
		return avatarurl;
	}
	public void setAvatarurl(String avatarurl) {
		this.avatarurl = avatarurl;
	}
	public Integer getPhone() {
		return phone;
	}
	public void setPhone(Integer phone) {
		this.phone = phone;
	}
	public String getLogintime() {
		return logintime;
	}
	public void setLogintime(String logintime) {
		this.logintime = logintime;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public List<Aging> getWatermark() {
		return watermark;
	}
	public void setWatermark(List<Aging> watermark) {
		this.watermark = watermark;
	}
	public WxUser(Integer id, String nickname, String gender, String country, String avatarurl, Integer phone,
			String logintime, String openid, List<Aging> watermark) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.gender = gender;
		this.country = country;
		this.avatarurl = avatarurl;
		this.phone = phone;
		this.logintime = logintime;
		this.openid = openid;
		this.watermark = watermark;
	}
	public WxUser() {
		super();
	}
	@Override
	public String toString() {
		return "WxUser [id=" + id + ", nickname=" + nickname + ", gender=" + gender + ", country=" + country
				+ ", avatarurl=" + avatarurl + ", phone=" + phone + ", logintime=" + logintime + ", openid=" + openid
				+ ", watermark=" + watermark + "]";
	}
	
	
}
