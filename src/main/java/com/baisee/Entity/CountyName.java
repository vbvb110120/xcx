package com.baisee.Entity;

public class CountyName {
	private String department;
	private String orgname;
	private String orgid;
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getOrgname() {
		return orgname;
	}
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}

	public String getOrgid() {
		return orgid;
	}

	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}

	public CountyName(String department, String orgname) {
		super();
		this.department = department;
		this.orgname = orgname;
	}
	public CountyName() {
		super();
	}
	@Override
	public String toString() {
		return "CountyName [department=" + department + ", orgname=" + orgname + "]";
	}
}
