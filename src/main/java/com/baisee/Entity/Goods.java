package com.baisee.Entity;

public class Goods {
	private Integer good_id;
	private String good;
	public Integer getGood_id() {
		return good_id;
	}
	public void setGood_id(Integer good_id) {
		this.good_id = good_id;
	}
	public String getGood() {
		return good;
	}
	public void setGood(String good) {
		this.good = good;
	}
	public Goods(Integer good_id, String good) {
		super();
		this.good_id = good_id;
		this.good = good;
	}
	public Goods() {
		super();
	}
	@Override
	public String toString() {
		return "Goods [good_id=" + good_id + ", good=" + good + "]";
	}
	
}
