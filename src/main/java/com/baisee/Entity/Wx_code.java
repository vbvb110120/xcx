package com.baisee.Entity;

public class Wx_code {
	private static final String appid="wxe5176ddb6809bffe";
	private static final String secret="c2ca22ada527b8f1141df5b821d9cf65";
	private String openid;
	private String session_key;
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getSession_key() {
		return session_key;
	}
	public void setSession_key(String session_key) {
		this.session_key = session_key;
	}
	public Wx_code(String openid, String session_key) {
		super();
		this.openid = openid;
		this.session_key = session_key;
	}
	public Wx_code() {
		super();
	}
	@Override
	public String toString() {
		return "Wx_code [openid=" + openid + ", session_key=" + session_key + "]";
	}
	public static String getAppid() {
		return appid;
	}
	public static String getSecret() {
		return secret;
	}
	
	
}
