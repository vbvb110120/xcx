package com.baisee.Entity;

public class Channel {
	private Integer channel_id;
	private String channel;
	public Integer getChannel_id() {
		return channel_id;
	}
	public void setChannel_id(Integer channel_id) {
		this.channel_id = channel_id;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public Channel(Integer channel_id, String channel) {
		super();
		this.channel_id = channel_id;
		this.channel = channel;
	}
	public Channel() {
		super();
	}
	@Override
	public String toString() {
		return "Channel [channel_id=" + channel_id + ", channel=" + channel + "]";
	}
	
}
