package com.baisee.Entity;

public class Consumption {
    private Integer pcs_id;
    private String unitname;
    private String province;
    private String content;
    private Double approval_money;
    private Double investment_money;
    private String results;
    private String remarks;
    private String time;

    public Integer getPcs_id() {
        return pcs_id;
    }

    public void setPcs_id(Integer pcs_id) {
        this.pcs_id = pcs_id;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getApproval_money() {
        return approval_money;
    }

    public void setApproval_money(Double approval_money) {
        this.approval_money = approval_money;
    }

    public Double getInvestment_money() {
        return investment_money;
    }

    public void setInvestment_money(Double investment_money) {
        this.investment_money = investment_money;
    }

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
