package com.baisee.application;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@SpringBootApplication(scanBasePackages = "com.baisee")
@EnableAutoConfiguration(exclude = { MultipartAutoConfiguration.class })
@MapperScan({"com.baisee.Mapper"})
@EnableCaching
public class NFH_Application {
	public static void main(String[] args) {
		SpringApplication.run(NFH_Application.class, args);
	}
	//解决上传文件控制层接收为null的方法
		@Bean(name = "multipartResolver")
		public CommonsMultipartResolver multipartResolver() {
	       CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
	       return multipartResolver;
	    }
		/**
		    * http重定向到https
		    * @return
		    */
		  /* @Bean
		   public TomcatServletWebServerFactory servletContainer() {
		      TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
		         @Override
		         protected void postProcessContext(Context context) {
		            SecurityConstraint constraint = new SecurityConstraint();
		            constraint.setUserConstraint("CONFIDENTIAL");
		            SecurityCollection collection = new SecurityCollection();
		            collection.addPattern("/*");
		            constraint.addCollection(collection);
		            context.addConstraint(constraint);
		         }
		      };
		      tomcat.addAdditionalTomcatConnectors(httpConnector());
		      return tomcat;
		   }

		   @Bean
		   public Connector httpConnector() {
		      Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
		      connector.setScheme("http");
		      //Connector监听的http的端口号
		      connector.setPort(80);
		      connector.setSecure(false);
		      //监听到http的端口号后转向到的https的端口号
		      connector.setRedirectPort(443);
		      return connector;
		   }*/
		   

}
