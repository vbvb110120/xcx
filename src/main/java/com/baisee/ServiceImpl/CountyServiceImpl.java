package com.baisee.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.baisee.Entity.County;
import com.baisee.Entity.CountyName;
import com.baisee.Mapper.CountyMapper;
import com.baisee.Service.CountyService;

@Service
public class CountyServiceImpl implements CountyService {
	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;
	
	@Autowired
	private CountyMapper mapper;

	@Override
	public List<County> findCounty() {
		System.out.println(url);
		System.out.println(username);
		System.out.println(password);
		return mapper.findCounty();
	}

	@Override
	public List<County> findByid(String id) {

		return mapper.findByid(id);
	}

	@Override
	public List<CountyName> findByname(String name) {
		return mapper.findByname(name);
	}

	@Override
	public List<CountyName> findByname1(String name) {
		return mapper.findByname1(name);
	}
}