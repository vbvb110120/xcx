package com.baisee.ServiceImpl;

import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baisee.Entity.Goods;
import com.baisee.Mapper.GoodsMapper;
import com.baisee.Service.GoodsService;
@Service
public class GoodsServiceImpl implements GoodsService {
	@Autowired
	private GoodsMapper mapper;

	@Override
	public List<Goods> findGoods() {
		return mapper.findGoods();
	}
}
