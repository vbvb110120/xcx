package com.baisee.ServiceImpl;

import java.util.List;

import com.baisee.Entity.XcxUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baisee.Entity.Statistics;
import com.baisee.Mapper.StatisticsMapper;
import com.baisee.Service.StatisticsService;

@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private StatisticsMapper mapper;
    @Override
    public List<Statistics> orderByProvince(){
        return mapper.orderByProvince();
    }


    @Override
    public List<Statistics> selectByProvince(String province) {
        return mapper.selectByProvince(province);
    }

    @Override
    public List<XcxUser> selectBindByDepartment(String orgname, String department) {
        return mapper.selectBindByDepartment(orgname,department);
    }

    @Override
    public List<XcxUser> selectBindByOrgname(String orgname) {
        return mapper.selectBindByOrgname(orgname);
    }

    @Override
    public List<XcxUser> selectBindByTotal(String orgname, String department, String total) {
        return mapper.selectBindByTotal(orgname,department,total);
    }

    @Override
    public List<Statistics> department(String type) {
        return mapper.department(type);
    }

    @Override
    public List<Statistics> total(String type) {
        return mapper.total(type);
    }

    @Override
    public List<Statistics> orderByArea1(String area) {
        return mapper.orderByArea1(area);
    }
    @Override
    public List<Statistics> orderByArea2(String area) {
        return mapper.orderByArea2(area);
    }


}
