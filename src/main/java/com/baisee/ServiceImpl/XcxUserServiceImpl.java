package com.baisee.ServiceImpl;

import com.baisee.Entity.XcxUser;
import com.baisee.Mapper.XcxUserMapper;
import com.baisee.Service.XcxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class XcxUserServiceImpl implements XcxUserService {
    @Autowired
    private XcxUserMapper xcxUserMapper;
    @Override
    public void insertRecord(XcxUser user){
        xcxUserMapper.insertRecord(user);
    }

    @Override
    public List<XcxUser> selectByOpenid(String openId, int i){
        return xcxUserMapper.selectByOpenid(openId,i);
    }
    @Override
    public void deleteRecord(String username, String unitname){
        xcxUserMapper.deleteRecord(username,unitname);
    }

    @Override
    public void updateRecord(XcxUser user) {
        xcxUserMapper.updateRecord(user);
    }

    @Override
    public void updateRecord1(String username, String unitname) {
        xcxUserMapper.updateRecord1(username,unitname);
    }

    @Override
    public List<XcxUser> selectNoDepartmentOrNoTotal() {
        return xcxUserMapper.selectNoDepartmentOrNoTotal();
    }

    @Override
    public void updateById(XcxUser user) {
        xcxUserMapper.updateById(user);
    }

}
