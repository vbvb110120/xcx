package com.baisee.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baisee.Entity.WxUser;
import com.baisee.Mapper.LoginMapper;
import com.baisee.Service.LoginService;
@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginMapper mapper;
	@Override
	public Integer findUserByOpenId(String open_id) {
		return mapper.findUserByOpenId(open_id);
	}
	@Override
	public void insertUser(WxUser user) {
		mapper.insertUser(user);
	}
	@Override
	public void updateLogintime(WxUser user) {
		mapper.updateLogintime(user);
	}

}
