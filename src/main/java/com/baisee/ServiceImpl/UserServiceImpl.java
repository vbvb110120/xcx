package com.baisee.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baisee.Entity.Channel;
import com.baisee.Entity.User;
import com.baisee.Mapper.ChannelMapper;
import com.baisee.Mapper.UserMapper;
import com.baisee.Service.ChannelService;
import com.baisee.Service.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper mapper;

	@Override
	public String findByName(String name) {
		return mapper.findByName(name);
	}
	

}
