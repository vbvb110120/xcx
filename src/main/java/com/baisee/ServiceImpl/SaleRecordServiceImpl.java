package com.baisee.ServiceImpl;

import com.baisee.Entity.Sale;
import com.baisee.Mapper.SaleMapper;
import com.baisee.Service.SaleRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class SaleRecordServiceImpl implements SaleRecordService {
    @Autowired
    private SaleMapper mapper;
    @Override
    public List<Sale> findRecordList() {
        return mapper.findRecordList();
    }
    @Override
    public void insertRecord(Sale sale){
        mapper.insertRecord(sale);
    }
    @Override
    public List<Sale> findRecordByName(String username, String unitname,String phone ){
        return mapper.findRecordByName(username,unitname,phone);
    }
    @Override
    public void deleteRecord(Integer id){
        mapper.deleteRecord(id);
    }

    @Override
    public void updateByName(String username, String phone) {
        mapper.updateByName(username,phone);
    }

    @Override
    public void updateById(Sale sale) {
        mapper.updateById(sale);
    }
}
