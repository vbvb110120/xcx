package com.baisee.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baisee.Entity.Channel;
import com.baisee.Mapper.ChannelMapper;
import com.baisee.Service.ChannelService;
@Service
public class ChannelServiceImpl implements ChannelService {
	@Autowired
	private ChannelMapper mapper;
	public List<Channel> findChannel() {
		return mapper.findChannel();
	}

}
