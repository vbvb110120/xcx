package com.baisee.ServiceImpl;

import com.baisee.Entity.Consumption;
import com.baisee.Mapper.ConsumptionMapper;
import com.baisee.Service.ConsumptionRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class ConsumptionRecordServiceImpl implements ConsumptionRecordService {
    @Autowired
    private ConsumptionMapper mapper;
    @Override
    public List<Consumption> findRecordList() {
        return mapper.findRecordList();
    }
}
