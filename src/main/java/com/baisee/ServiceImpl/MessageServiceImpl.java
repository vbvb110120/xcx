package com.baisee.ServiceImpl;

import com.baisee.Mapper.MessageMapper;
import com.baisee.Service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    MessageMapper messageMpper;
    @Override
    public void insertMessage(String Msg){
        messageMpper.insertMessage(Msg);
    }
}
