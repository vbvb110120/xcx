package com.baisee.ServiceImpl;

import com.baisee.Mapper.CityInfoMapper;
import com.baisee.Service.CityInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityInfoServiceImpl implements CityInfoService {
    @Autowired
    private CityInfoMapper cityInfoMapper;
    public String[] SelectFirstLevel(){
        return cityInfoMapper.SelectFirstLevel();
    }
    public String[] SelectSecondLevel(String FirstName) {
        return cityInfoMapper.SelectSecondLevel(FirstName);
    }
    public String[] SelectUnitName(String SecondName){
        return cityInfoMapper.SelectUnitName(SecondName);
    }

    @Override
    public String isProvince(String string) {
        return cityInfoMapper.isProvince(string);
    }

    @Override
    public String[] findOrgname1(String unit_name) {
        return cityInfoMapper.findOrgname1(unit_name);
    }

    @Override
    public String[] findOrgname2() {
        return cityInfoMapper.findOrgname2();
    }

    @Override
    public String[] findTotal(String department) {
        return cityInfoMapper.findTotal(department);
    }
}
