package com.baisee.ServiceImpl;

import com.baisee.Entity.Purchase;
import com.baisee.Mapper.PurchaseMapper;
import com.baisee.Service.PurchaseRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class PurchaseRecordServiceImpl implements PurchaseRecordService {
    @Autowired
    private PurchaseMapper mapper;
    @Override
    public List<Purchase> findRecordList() {
        return mapper.findRecordList();
    }
    @Override
    public void insertRecord(Purchase purchase){
        mapper.insertRecord(purchase);
    }
    @Override
    public void deleteRecord(Integer id){
        mapper.deleteRecord(id);
    }

    @Override
    public void updateByName(String username, String unitname) {
        mapper.updateByName(username,unitname);
    }

    @Override
    public void updateById(Purchase purchase) {
        mapper.updateById(purchase);
    }

    @Override
    public List<Purchase> findRecordByTotal(String username, String unitname, String department) {
        return mapper.findRecordByTotal(username,unitname,department);
    }

    @Override
    public List<Purchase> findRecordByDepartment(String unitname) {
        return mapper.findRecordByDepartment(unitname);
    }

    @Override
    public List<Purchase> selectNoDepartmentOrNoTotal(String branch, String name, String phone) {
        return mapper.selectNoDepartmentOrNoTotal(branch,name,phone);
    }

//    @Override
//    public List<Purchase> findRecordByDepartment(String username, String unitname) {
//        return mapper.findRecordByDepartment(username,unitname);
//    }

    @Override
    public List<Purchase> findRecordByName(String username,String unitname,String phone){
        return mapper.findRecordByName(username,unitname,phone);
    }

}
