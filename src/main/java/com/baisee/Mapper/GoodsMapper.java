package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.poi.ss.formula.functions.T;

import com.baisee.Entity.Channel;
import com.baisee.Entity.Goods;

@Mapper
public interface GoodsMapper {

	List<Channel> findChannel();

	List<Goods> findGoods();

}
