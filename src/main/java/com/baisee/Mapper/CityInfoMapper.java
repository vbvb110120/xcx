package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CityInfoMapper {
    String[] SelectFirstLevel();
    String[] SelectSecondLevel(String FirstName);
    String[] SelectUnitName(String SecondName);

    String isProvince(String string);

    String[] findOrgname1(String unit_name);

    String[] findOrgname2();

    String[] findTotal(String department);
}
