package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baisee.Entity.Channel;
import com.baisee.Entity.User;

@Mapper
public interface UserMapper {

	String findByName(String name);

}
