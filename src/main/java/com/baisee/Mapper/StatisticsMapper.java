package com.baisee.Mapper;

import com.baisee.Entity.Statistics;
import com.baisee.Entity.XcxUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StatisticsMapper {
    List<Statistics> orderByProvince();

    List<Statistics> selectByProvince(String province);


    List<XcxUser> selectBindByDepartment(String orgname, String department);


    List<XcxUser> selectBindByOrgname(String orgname);

    List<XcxUser> selectBindByTotal(String orgname, String department, String total);

    List<Statistics> department(String type);

    List<Statistics> total(String type);

    List<Statistics> orderByArea1(String area);

    List<Statistics> orderByArea2(String area);
}
