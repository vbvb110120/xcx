package com.baisee.Mapper;

import com.baisee.Entity.Consumption;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ConsumptionMapper {
    List<Consumption> findRecordList();
}
