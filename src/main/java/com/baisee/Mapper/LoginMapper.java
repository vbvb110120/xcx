package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baisee.Entity.WxUser;

@Mapper
public interface LoginMapper {

	Integer findUserByOpenId(String open_id);

	void insertUser(WxUser user);

	void updateLogintime(WxUser user);

}
