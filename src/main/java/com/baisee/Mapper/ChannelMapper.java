package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baisee.Entity.Channel;

@Mapper
public interface ChannelMapper {

	List<Channel> findChannel();

}
