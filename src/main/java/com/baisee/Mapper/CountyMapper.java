package com.baisee.Mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;

import com.baisee.Entity.County;
import com.baisee.Entity.CountyName;

@Mapper
public interface CountyMapper {

	List<County> findCounty();

	List<County> findByid(String id);

	List<CountyName> findByname(@Param("name") String name);
	List<CountyName> findByname1(@Param("name") String name);

}
