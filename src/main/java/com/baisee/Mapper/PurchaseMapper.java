package com.baisee.Mapper;

import com.baisee.Entity.Purchase;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PurchaseMapper {
    List<Purchase> findRecordList();
    List<Purchase> findRecordByName(String username,String unitname,String phone);
    void insertRecord(Purchase purchase);
    void deleteRecord(Integer id);

    void updateByName(String username, String unitname);

    void updateById(Purchase purchase);

    List<Purchase> findRecordByTotal(String username, String unitname, String department);

//    List<Purchase> findRecordByDepartment(String username, String unitname);

    List<Purchase> findRecordByDepartment(String unitname);

    List<Purchase> selectNoDepartmentOrNoTotal(String branch, String name, String phone);
}
