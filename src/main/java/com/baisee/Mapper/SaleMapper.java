package com.baisee.Mapper;

import com.baisee.Entity.Sale;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SaleMapper {
    List<Sale> findRecordList();

    void insertRecord(Sale sale);
    void deleteRecord(Integer id);

    List<Sale> findRecordByName(String username, String unitname, String phone);

    void updateByName(String username, String phone);

    void updateById(Sale sale);
}
