package com.baisee.Mapper;

import com.baisee.Entity.XcxUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface XcxUserMapper {
    public void insertRecord(XcxUser user);
    public List<XcxUser> selectByOpenid(String openId, int i);

    void deleteRecord(String username, String unitname);

    void updateRecord(XcxUser user);

    void updateRecord1(String username, String unitname);

    List<XcxUser> selectNoDepartmentOrNoTotal();

    void updateById(XcxUser user);
}
