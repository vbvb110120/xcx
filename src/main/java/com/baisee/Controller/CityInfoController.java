package com.baisee.Controller;

import com.baisee.Service.CityInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("cityinfo")
public class CityInfoController {
    private final CityInfoService cityInfoService;

    public CityInfoController(CityInfoService cityInfoService) {
        this.cityInfoService = cityInfoService;
    }

    @RequestMapping("firstlevel")
    public String[] getFirstLevel(){
        return cityInfoService.SelectFirstLevel();
    }

    @RequestMapping("secondlevel")
    public String[] getSecondLevel(HttpServletRequest request){
        String firstLevel=request.getParameter("province");
        System.out.println(firstLevel);
        return cityInfoService.SelectSecondLevel(firstLevel);
    }

    @RequestMapping("unitname")
    public String[] getUnitName(HttpServletRequest request){
        String secondLevel=request.getParameter("point");
        String[] strings=cityInfoService.SelectUnitName(secondLevel);
        String[] strings1=new String[strings.length+1];
        System.arraycopy(strings, 0, strings1, 0, strings.length);
        strings1[strings.length]="0";
        if(strings.length==1){
            String unit_code=cityInfoService.isProvince(strings[0]);
            if(unit_code.contains("0000999"))
                strings1[strings.length]="1";
        }
        return strings1;
    }
    /*返回部门列表*/
    @RequestMapping("department")
    public String[] getDepartment(HttpServletRequest request){
        String unit_name=request.getParameter("branch");
        System.out.println(unit_name);
        if(unit_name.equals("中国农业发展银行总行")){
            return cityInfoService.findOrgname2();
        }
        return cityInfoService.findOrgname1(unit_name);
    }
    /*返回处室*/
    @RequestMapping("total")
    public String[] getTotal(HttpServletRequest request){
        String department=request.getParameter("department");
        System.out.println(department);
        return cityInfoService.findTotal(department);
    }
}
