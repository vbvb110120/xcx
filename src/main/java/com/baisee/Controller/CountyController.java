package com.baisee.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baisee.Entity.County;
import com.baisee.Entity.CountyName;
import com.baisee.Service.CountyService;

@RequestMapping("County")
@Controller
public class CountyController {
	private final CountyService service;

	public CountyController(CountyService service) {
		this.service = service;
	}

	@RequestMapping("findCounty")
	@ResponseBody
	public List<County> FindCounty(){
		return service.findCounty();
	}
	@RequestMapping("findByid")
	@ResponseBody
	public List<County> findByid(HttpServletRequest req){
		return service.findByid(req.getParameter("id"));
	}
	@RequestMapping("findByname")
	@ResponseBody
	public List<CountyName> findByname(@Param("name") String name){
		return service.findByname(name);
	}
}
