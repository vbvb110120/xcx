package com.baisee.Controller;

import com.baisee.Entity.Statistics;
import com.baisee.Entity.XcxUser;
import com.baisee.Service.StatisticsService;
import com.baisee.Utils.ExcelUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RequestMapping("statistics")
@RestController
public class StatisticsController {
    private final StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    /*按省统计各省采购和帮销汇总结果*/
    @RequestMapping("collect")
    @ResponseBody
    public String province(HttpServletRequest request) throws IOException {
        String[] title;
        String[][] content;
        String filename;
        String sheetName = "sheet1";
        int j=0;
        double sumSingle=0.00;
        double sumUnit=0.00;
        double sumSale=0.00;
        double sumBuy=0.00;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String type=request.getParameter("type");
        List<Statistics> list;
        String area=request.getParameter("area");
        switch (type) {
            /*按省统计*/
            case "province":
                title = new String[]{"序号", "省级分行", "个人购买产品金额", "机构购买产品金额", "帮助销售产品金额", "购买小计", "合计"};
                filename = "province" + sdf.format(new Date()) + ".xls";
                list = statisticsService.orderByProvince();
                break;
            /*按市统计*/
            case "city":
                sdf = new SimpleDateFormat("hhmmssSSS");
                title = new String[]{"序号", "市级分行", "个人购买产品金额", "机构购买产品金额", "帮助销售产品金额", "购买小计", "合计"};
                filename = "city" + sdf.format(new Date()) + ".xls";
                list = statisticsService.orderByArea1(area);
                break;
            /*按县统计*/
            case "country":
                sdf = new SimpleDateFormat("hhmmssSSS");
                title = new String[]{"序号", "县级分行", "个人购买产品金额", "机构购买产品金额", "帮助销售产品金额", "购买小计", "合计"};
                filename = "country" + sdf.format(new Date()) + ".xls";
                list = statisticsService.orderByArea2(area);
                break;
            /*总行部门统计*/
            case "中国农业发展银行总行":
                filename = "bumentongji" + sdf.format(new Date()) + ".xls";
                title = new String[]{"序号", "部门", "个人购买产品金额", "机构购买产品金额", "帮助销售产品金额", "购买小计", "合计"};
                list = statisticsService.department(type);
                break;
            default:
                /*按处室统计*/
                sdf = new SimpleDateFormat("hhmmssSSS");
                filename = "chushitongji" + sdf.format(new Date()) + ".xls";
                title = new String[]{"序号", "处室", "个人购买产品金额", "机构购买产品金额", "帮助销售产品金额", "购买小计", "合计"};
                list = statisticsService.total(type);
                break;
        }
        content = new String[list.size()+10][10];
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getUsertype().equals("其他"))
                continue;
            if(list.get(i).getUsertype().equals("机构")&&
              ((i==0)||((!list.get(i).getProvince().equals(list.get(i - 1).getProvince()))))){
                content[j][0] = String.valueOf(j+1);
                content[j][1] = list.get(i).getProvince();
                content[j][2] = "0.00";
                content[j][3] = String.format("%.2f",list.get(i).getSum());
                content[j][4] = list.get(i).getSale()>0?String.format("%.2f",list.get(i).getSale()):"0.00";
                content[j][5] = String.format("%.2f",list.get(i).getSum());
                content[j][6] = String.format("%.2f",list.get(i).getSum()+list.get(i).getSale());
                sumUnit+=list.get(i).getSum();
                sumSale+=list.get(i).getSale();
                sumBuy+=list.get(i).getSum();
                j++;
                continue;
            }
            else if(i>0&&list.get(i).getProvince().equals(list.get(i-1).getProvince())){
                content[j-1][3] = String.format("%.2f",list.get(i).getSum());
                content[j-1][4] = list.get(i).getSale()>0?String.format("%.2f",list.get(i).getSale()):"0.00";
                content[j-1][5] = String.format("%.2f",list.get(i).getSum()+Double.parseDouble(content[j-1][5]));
                content[j-1][6] = String.format("%.2f",Double.parseDouble(content[j-1][5])+Double.parseDouble(content[j-1][4]));
                sumUnit+=list.get(i).getSum();
                sumSale+=list.get(i).getSale();
                sumBuy+=list.get(i).getSum();
                continue;
            }
            content[j][0] = String.valueOf(j+1);
            content[j][1] = list.get(i).getProvince();
            content[j][2] = String.format("%.2f",list.get(i).getSum());
            content[j][3] = "0.00";
            content[j][4] = "0.00";
            content[j][5] = String.format("%.2f",list.get(i).getSum());
            content[j][6] = String.format("%.2f",list.get(i).getSum());
            sumSingle+=list.get(i).getSum();
            sumBuy+=list.get(i).getSum();
            j++;
        }
        content[j][0]="总计";
        content[j][2]=String.format("%.2f",sumSingle);
        content[j][3]=String.format("%.2f",sumUnit);
        content[j][4]=String.format("%.2f",sumSale);
        content[j][5]=String.format("%.2f",sumBuy);
        content[j][6]=String.format("%.2f",sumSale+sumBuy);
        content[j+3][0] = "注：用户绑定信息错误可能导致统";
        content[j+3][1] = "计数据不准，如出现该情况，请相";
        content[j+3][2] = "关机构/部门/处室自查用户绑定信";
        content[j+3][3] = "息,对定位不清的用户重新绑定,如";
        content[j+3][4] = "有遗漏未报的用户请尽快绑定并填";
        content[j+3][5] = "报。";
        return ExcelUtil.export(sheetName,title,content,filename);
    }

    /*sql结果返回当前部门处室绑定人员情况*/
    @RequestMapping("bind")
    @ResponseBody
    public String bind(HttpServletRequest request) throws IOException {
        String orgname=request.getParameter("branch");
        String department=request.getParameter("department");
        String total=request.getParameter("total");
        List<XcxUser> list=null;
        SimpleDateFormat sdf = new SimpleDateFormat("hhmmssSSS");
        String filename="bind"+sdf.format(new Date()) + ".xls";
        String sheetName = "sheet1";
        String[] title = new String[]{"序号", "机构", "部门","处室","姓名"};
        if(total!=null){
            list=statisticsService.selectBindByTotal(orgname,department,total);
        }
        if(department!=null&&total==null){
            list=statisticsService.selectBindByDepartment(orgname,department);
        }
        if(orgname!=null&department==null){
            list=statisticsService.selectBindByOrgname(orgname);
        }
        assert list != null;
        String[][] content = new String[list.size()+10][10];
        for (int i = 0; i < list.size(); i++) {
            content[i][0] = String.valueOf(i+1);
            content[i][1] = list.get(i).getBranch();
            content[i][2] = list.get(i).getDepartment();
            content[i][3] = list.get(i).getTotal();
            content[i][4] = list.get(i).getName();
        }
        return ExcelUtil.export(sheetName,title,content,filename);
    }
}
