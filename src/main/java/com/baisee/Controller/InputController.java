package com.baisee.Controller;


import com.baisee.Entity.Purchase;
import com.baisee.Entity.Sale;
import com.baisee.Service.MessageService;
import com.baisee.Service.PurchaseRecordService;
import com.baisee.Service.SaleRecordService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
public class InputController {
    private final PurchaseRecordService purchaseRecordService;
    private final SaleRecordService saleRecordService;
    private final MessageService messageService;

    public InputController(PurchaseRecordService purchaseRecordService, SaleRecordService saleRecordService, MessageService messageService) {
        this.purchaseRecordService = purchaseRecordService;
        this.saleRecordService = saleRecordService;
        this.messageService = messageService;
    }

    @RequestMapping("purchase/input")
    @ResponseBody
    public String purchaseInput(HttpServletRequest req) {
        Purchase purchase=new Purchase();
        String jstring=req.getParameter("list");
        System.out.println("jstring="+jstring);
        System.out.println(System.currentTimeMillis());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(new Date());
        JSONObject object=JSONObject.fromObject(jstring);

        purchase.setUsername(object.get("username").toString());
        purchase.setUsertype(object.get("usertype").toString());
        purchase.setDepartment(object.get("department").toString());
        purchase.setTotal(object.get("total").toString());
        purchase.setRemarks(object.get("remarks").toString());
        purchase.setTime(date);
        purchase.setPhone(object.get("phone").toString());
        purchase.setFirstname(object.get("firstname").toString());
        purchase.setSecondname(object.get("secondname").toString());
        purchase.setUnitname(object.get("unitname").toString());
        JSONArray data=object.getJSONArray("data");
        if(data.size()>0){
            for(int i=0;i<data.size();i++){
                JSONObject job = data.getJSONObject(i);
                purchase.setContent(job.get("content").toString());
                purchase.setNumber(job.get("number").toString());
                purchase.setGood(job.get("good").toString());
                purchase.setMoney(Double.parseDouble(job.get("money").toString()));
                purchase.setMode_id(job.get("mode_id").toString());
                purchase.setCounty(job.get("county").toString());
                purchase.setChannel(job.get("channel").toString());
                purchaseRecordService.insertRecord(purchase);
            }
        }
        return "操作成功";
    }

    @RequestMapping("purchase/delete")
    @ResponseBody
    public String purchaseDelete(HttpServletRequest req) {
        System.out.println(req.getParameter("id"));
        Integer id=Integer.parseInt(req.getParameter("id"));
        purchaseRecordService.deleteRecord(id);
        return "操作成功";
    }

    @RequestMapping("purchase/findbyname")
    @ResponseBody
    public List<Purchase> findbyname(HttpServletRequest req) {
        String username=req.getParameter("username");
        String unitname=req.getParameter("unitname");
        String phone=req.getParameter("phone");
        return purchaseRecordService.findRecordByName(username,unitname,phone);
    }


    @RequestMapping("sale/input")
    @ResponseBody
    public String saleInput(HttpServletRequest req) {
        Sale sale=new Sale();
        String jstring=req.getParameter("list");
        System.out.println("jstring="+jstring);
        System.out.println(System.currentTimeMillis());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = df.format(new Date());
        JSONObject object=JSONObject.fromObject(jstring);

        sale.setPurchaser(object.get("purname").toString());
        sale.setSupporter(object.get("supname").toString());
        sale.setUsername(object.get("username").toString());
        sale.setDepartment(object.get("department").toString());
        sale.setTotal(object.get("total").toString());
        sale.setRemarks(object.get("remarks").toString());
        sale.setTime(date);
        sale.setPhone(object.get("phone").toString());
        sale.setFirstname(object.get("firstname").toString());
        sale.setSecondname(object.get("secondname").toString());
        sale.setUnitname(object.get("unitname").toString());
        JSONArray data=object.getJSONArray("data");
        if(data.size()>0){
            for(int i=0;i<data.size();i++){
                JSONObject job = data.getJSONObject(i);
                sale.setContent(job.get("content").toString());
                sale.setNumber(Integer.parseInt(job.get("number").toString()));
                sale.setGood(job.get("good").toString());
                sale.setMoney(Double.parseDouble(job.get("money").toString()));
                sale.setMode_id(job.get("mode_id").toString());
                sale.setCounty(job.get("county").toString());
                sale.setChannel(job.get("channel").toString());
                System.out.println(sale.getChannel());
                saleRecordService.insertRecord(sale);
            }
        }
        return "操作成功";
    }

    @RequestMapping("sale/delete")
    @ResponseBody
    public String saleDelete(HttpServletRequest req) {
        Integer id=Integer.parseInt(req.getParameter("id"));
        saleRecordService.deleteRecord(id);
        return "操作成功";
    }
    @RequestMapping("sale/findbyname")
    @ResponseBody
    public List<Sale> Salefindbyname(HttpServletRequest req) {
        String username=req.getParameter("username");
        String unitname=req.getParameter("unitname");
        String phone=req.getParameter("phone");
        System.out.println(username);
        System.out.println(unitname);
        return saleRecordService.findRecordByName(username,unitname,phone);
    }

    @RequestMapping("message")
    @ResponseBody
    public String message(HttpServletRequest req) {
        String Msg=req.getParameter("msg");
        messageService.insertMessage(Msg);
        return "操作成功";
    }
}
