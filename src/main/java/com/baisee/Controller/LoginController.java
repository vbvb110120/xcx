package com.baisee.Controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.baisee.Entity.*;
import com.baisee.Service.PurchaseRecordService;
import com.baisee.Service.SaleRecordService;
import com.baisee.Service.XcxUserService;
import net.sf.json.JSONObject;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baisee.Service.LoginService;
import com.baisee.Utils.AESUtils;
import com.baisee.Utils.HttpClientUtil;
import com.baisee.Utils.JSONFormUtil;
import com.baisee.Utils.Session_idUtil;
import com.baisee.redis.RedisUtilsService;


/**
 * 登录校验层
 * 
 * @author wyh
 *
 */
@RestController
@RequestMapping("/Login")
public class LoginController {
	final
	RedisUtilsService redis;
	private final LoginService service;
	private final XcxUserService userservice;
	private final PurchaseRecordService purchaseRecordService;
	private final SaleRecordService saleRecordService;

	public LoginController(RedisUtilsService redis, LoginService service, XcxUserService userservice, PurchaseRecordService purchaseRecordService, SaleRecordService saleRecordService) {
		this.redis = redis;
		this.service = service;
		this.userservice = userservice;
		this.purchaseRecordService = purchaseRecordService;
		this.saleRecordService = saleRecordService;
	}

	/*
	* 将首次登陆客户进行绑定
	*/
	@RequestMapping("/bind")
	@ResponseBody
	public String bind(HttpServletRequest request) {
		XcxUser user=new XcxUser();
		String data=request.getParameter("data");
		JSONObject object= JSONObject.fromObject(data);
		user.setBranch(object.getString("branch"));
		user.setDepartment(object.getString("department"));
		user.setName(object.getString("name"));
		user.setPhone(object.getString("phone"));
		user.setPoint(object.getString("point"));
		user.setProvince(object.getString("province"));
		user.setTotal(object.getString("total"));
		String skey=object.getString("skey");
		Object obj=redis.get(skey);
		Map map = com.alibaba.fastjson.JSONObject.parseObject(com.alibaba.fastjson.JSONObject.toJSONString(obj), Map.class);
		String openId = map.get("openid").toString();
		if(openId==null){
			System.out.println("map="+map);
			return "openId为空";
		}
		user.setOpen_id(openId);
		List<XcxUser> list=userservice.selectByOpenid(openId,1);
		if(list.size()>=1){
			user.setId(list.get(0).getId());
			userservice.updateRecord(user);
			List<Sale> list1=saleRecordService.findRecordByName(list.get(0).getName(),list.get(0).getBranch(),list.get(0).getPhone());
			for (Sale sale : list1) {
				sale.setUsername(user.getName());
				sale.setFirstname(user.getProvince());
				sale.setSecondname(user.getPoint());
				sale.setUnitname(user.getBranch());
				sale.setDepartment(user.getDepartment());
				sale.setTotal(user.getTotal());
				sale.setPhone(user.getPhone());
				saleRecordService.updateById(sale);
			}
			List<Purchase> list2=purchaseRecordService.findRecordByName(list.get(0).getName(),list.get(0).getBranch(),list.get(0).getPhone());
			for (Purchase purchase : list2) {
				purchase.setUsername(user.getName());
				purchase.setFirstname(user.getProvince());
				purchase.setSecondname(user.getPoint());
				purchase.setUnitname(user.getBranch());
				purchase.setDepartment(user.getDepartment());
				purchase.setTotal(user.getTotal());
				purchase.setPhone(user.getPhone());
				purchaseRecordService.updateById(purchase);
			}
		}
		else{
			userservice.insertRecord(user);
		}
		return "操作成功";
	}

	/*
	 * 用户解除绑定
	 */
	@RequestMapping("/cancelbind")
	@ResponseBody
	public String cancelbind(HttpServletRequest request) {
		String unitname=request.getParameter("unitname");
		String username=request.getParameter("username");
		userservice.updateRecord1(username,unitname);
		return "操作成功";
	}

	/*根据openid查看客户是否绑定过*/
	@RequestMapping("/CheckLogin")
	@ResponseBody
	public List<XcxUser> CheckLogin(@Param("skey") String skey){
		System.out.println("skey="+skey);
		Object obj=redis.get(skey);
		String openId;
		Map map = com.alibaba.fastjson.JSONObject.parseObject(com.alibaba.fastjson.JSONObject.toJSONString(obj), Map.class);
		if( map.get("openid")==null)
			return null;
		openId= map.get("openid").toString();
		return userservice.selectByOpenid(openId,0);
	}

	public Map getMap(String code) throws IOException {
		// 拼接url
		StringBuilder url = new StringBuilder("https://api.weixin.qq.com/sns/jscode2session?");
		url.append("appid=");// appid设置
		url.append(Wx_code.getAppid());
		url.append("&secret=");// secret设置
		url.append(Wx_code.getSecret());
		url.append("&js_code=");// code设置
		url.append(code);
		url.append("&grant_type=authorization_code");
		return HttpClientUtil.sendHttpClient(url);
	}
	/**
	 * 微信用户登录校验
	 */
	@RequestMapping(value = "/WxLogin")
	public String login(@Param("code") String code, HttpServletRequest request)
			throws IOException {

		Map map=getMap(code);
		String openid = (String) map.get("openid");
		String session_key = (String) map.get("session_key");
		Signature sig = new Signature();
		System.out.println("openid============================"+openid);
		System.out.println("123");
		sig.setOpenid(openid);
		sig.setSession_key(session_key);
		String encryptedData = AESUtils.weixinDecrypt(request.getParameter("encryptedData"), session_key,
				request.getParameter("iv"));
		System.err.println(encryptedData);
		// json字符串转对象
		WxUser list = JSONFormUtil.JSONtoObject(encryptedData, WxUser.class);
		System.err.println(list.toString());
		Integer id = service.findUserByOpenId(openid);
		WxUser user = new WxUser();
		Signature st = new Signature();
		// 没有则新增
		if (id == null) {
			user.setAvatarurl(list.getAvatarurl());
			user.setCountry(list.getCountry());
			user.setGender(gender(list.getGender()));
			user.setNickname(list.getNickname());
			Date date = new Date();
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String format = f.format(date);
			user.setLogintime(format);
			user.setOpenid(openid);
			service.insertUser(list);
			// 生成自定义登录态
			String skey = Session_idUtil.session_id();
			st.setOpenid(openid);
			st.setSession_key(session_key);
			// 存储
			redis.set(skey, st);
			return skey;
		} else {
			// 用户存在更新时间
			Date date = new Date();
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String format = f.format(date);
			user.setLogintime(format);
			user.setOpenid(openid);
			service.updateLogintime(user);
			st.setOpenid(openid);
			st.setSession_key(session_key);
			String skey = Session_idUtil.session_id();
			// 存储
			redis.set(skey, st);
			return skey;
		}
	}

	/**
	 * 简单姓名处理
	 */
	public String gender(String gender) {
		if (gender.equals("1")) {
			return "男";
		} else {
			return "女";
		}
	}
}
