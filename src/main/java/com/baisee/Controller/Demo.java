package com.baisee.Controller;


import com.baisee.Entity.CountyName;
import com.baisee.Entity.Purchase;
import com.baisee.Entity.XcxUser;
import com.baisee.Service.CityInfoService;
import com.baisee.Service.CountyService;
import com.baisee.Service.PurchaseRecordService;
import com.baisee.Service.XcxUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;
@Controller
public class Demo {
	private final XcxUserService xcxUserService;
	private final CountyService countyService;
	private final PurchaseRecordService purchaseRecordService;
	private final CityInfoService cityInfoService;
	public Demo(XcxUserService xcxUserService,PurchaseRecordService purchaseRecordService,CountyService countyService,CityInfoService cityInfoService) {
		this.xcxUserService = xcxUserService;
		this.countyService = countyService;
		this.purchaseRecordService = purchaseRecordService;
		this.cityInfoService = cityInfoService;
	}
	@RequestMapping("main1")
	public  void main1() {
		List<XcxUser> xcxUserList=xcxUserService.selectNoDepartmentOrNoTotal();
		List<CountyName> userList;
		int k=0;
		int j = 0;
		for (XcxUser user : xcxUserList) {
			userList=countyService.findByname1(user.getName());
			if(userList.size()==0) continue;
			if(userList.size()>0&&userList.size()<5){
				for(int i=0;i<userList.size();i++){
					if(userList.size()>1&&i<userList.size()-1&&userList.get(i).getOrgid().substring(0,2).equals(userList.get(i+1).getOrgid().substring(0,2))){
						break;
					}
					System.out.println(userList.get(i).getOrgname().substring(0,2));
					System.out.println(user.getBranch().substring(0,2));
					if(userList.get(i).getOrgname().substring(0,2).equals(user.getBranch().substring(0,2))||
							(userList.get(i).getOrgid().substring(0,2).equals("99")&&userList.get(i).getOrgid().substring(0,2).equals(cityInfoService.isProvince(user.getBranch()).substring(0,2)))){
						List<Purchase> purchaseList=purchaseRecordService.selectNoDepartmentOrNoTotal(user.getBranch(), user.getName(), user.getPhone());
						for (Purchase purchase:purchaseList){
							purchase.setDepartment(userList.get(i).getOrgname());
							purchase.setTotal(userList.get(i).getDepartment());
							purchaseRecordService.updateById(purchase);
							j++;
						}
						user.setDepartment(userList.get(i).getOrgname());
						user.setTotal(userList.get(i).getDepartment());
						xcxUserService.updateById(user);
						k++;
					}
				}
			}

		}
		System.out.println("修改purchase数:"+j);
		System.out.println("修改xcx_user数:"+k);
	}
}
