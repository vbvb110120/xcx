package com.baisee.Controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baisee.Entity.Channel;
import com.baisee.Service.ChannelService;
/**
 * 采购渠道控制层
 * @author wyh
 *
 */
@RequestMapping("Channel")
@Controller
public class ChannelController {
	private final ChannelService service;

	public ChannelController(ChannelService service) {
		this.service = service;
	}

	@RequestMapping("findChannel")
	@ResponseBody
	public List<Channel> FindCounty(){
		return service.findChannel();
	}
}
