package com.baisee.Controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baisee.Entity.Purchase;
import com.baisee.Entity.Sale;
import com.baisee.Service.PurchaseRecordService;
import com.baisee.Service.SaleRecordService;
import com.baisee.Utils.ExcelUtil;

@RestController
public class ExportController {
	private final PurchaseRecordService purchaseRecordService;
	private final SaleRecordService saleRecordService;

	public ExportController(PurchaseRecordService purchaseRecordService, SaleRecordService saleRecordService) {
		this.purchaseRecordService = purchaseRecordService;
		this.saleRecordService = saleRecordService;
	}

	/**
	 * 根据条件将数据导出为Excel 如果需要浏览器发送请求时即下载Excel，就不能用ajax进行传输，所以这里用GET方式进行提交
	 */
	@ResponseBody
	@RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
	public String exportExcel(HttpServletRequest request) throws IOException {
		String type = request.getParameter("type");
		String username = request.getParameter("username");
		String unitname = request.getParameter("unitname");
		String department=request.getParameter("department");
		String phone=request.getParameter("phone");
		String[] title = {};
		String[][] content = {};
		String filename = "";
		String sheetName = "sheet1";
		SimpleDateFormat sdf = new SimpleDateFormat("hhmmssSSS");
		double singlesum=0.00;
		double unitsum=0.00;
		double othersum=0.00;
		List<Purchase> list =null;
		switch (type) {
			case "purchase_single":
			case "purchase_total":
				switch (type) {
					case "purchase_single":
						list = purchaseRecordService.findRecordByName(username, unitname,phone);
						break;
					case "purchase_total":
						list = purchaseRecordService.findRecordByTotal(username, unitname, department);
						break;
				}
				title = new String[]{"ID", "机构/个人", "填报人", "一级分行名称", "二级分行名称", "机构名称", "部门", "处室", "采购种类", "采购内容", "采购数量", "采购金额", "定点/支援/其它", "扶贫对象", "采购渠道", "备注", "提交时间", "联系方式"};
				filename = "xiaoshou" + sdf.format(new Date()) + ".xls";
				content = new String[list.size() + 1][18];
				for (int i = 0; i < list.size(); i++) {
					content[i][0] = String.valueOf(i + 1);
					content[i][1] = list.get(i).getUsertype();
					if (list.get(i).getUsertype().equals("个人")) {
						singlesum += list.get(i).getMoney();
					}
					content[i][2] = list.get(i).getUsername();
					content[i][3] = list.get(i).getFirstname();
					content[i][4] = list.get(i).getSecondname();
					content[i][5] = list.get(i).getUnitname();
					content[i][6] = list.get(i).getDepartment();
					content[i][7] = list.get(i).getTotal();
					content[i][8] = list.get(i).getGood();
					content[i][9] = list.get(i).getContent();
					content[i][10] = String.valueOf(list.get(i).getNumber());
					content[i][11] = String.valueOf(list.get(i).getMoney());
					content[i][12] = list.get(i).getCounty();
					content[i][13] = list.get(i).getMode_id();
					content[i][14] = list.get(i).getChannel();
					content[i][15] = list.get(i).getRemarks();
					content[i][16] = list.get(i).getTime();
					content[i][17] = String.valueOf(list.get(i).getPhone());
				}
				content[list.size()][0] = "个人采购总计金额";
				content[list.size()][1] = String.format("%.2f", singlesum);
				break;
			case "purchase_department":
				title = new String[]{"ID", "机构/个人", "部门", "处室", "处室已采购人数", "采购金额"};
				list = purchaseRecordService.findRecordByDepartment(unitname);
				filename = "xiaoshou" + sdf.format(new Date()) + ".xls";
				content = new String[list.size() + 10][18];
				for (int i = 0; i < list.size(); i++) {
					content[i][0] = String.valueOf(i + 1);
					content[i][1] = list.get(i).getUsertype();
					content[i][2] = list.get(i).getDepartment();
					content[i][3] = list.get(i).getTotal();
					content[i][4] = String.valueOf(list.get(i).getCount());
					content[i][5] = String.format("%.2f", list.get(i).getSum());
					switch (list.get(i).getUsertype()) {
						case "个人":
							singlesum += list.get(i).getSum();
							break;
						case "机构":
							unitsum += list.get(i).getSum();
							break;
						case "其他":
							othersum += list.get(i).getSum();
							break;
					}
				}
				content[list.size()][0] = "个人采购总计金额";
				content[list.size()][5] = String.format("%.2f", singlesum);
				content[list.size() + 1][0] = "机构采购总计金额";
				content[list.size() + 1][5] = String.format("%.2f", unitsum);
				content[list.size() + 2][0] = "其他采购总计金额";
				content[list.size() + 2][5] = String.format("%.2f", othersum);
				break;
			case "sale":
				List<Sale> list1 = saleRecordService.findRecordByName(username, unitname,phone);
				title = new String[]{"ID", "填报人", "一级分行名称", "二级分行名称", "机构名称", "部门", "处室", "采购种类", "采购内容", "采购数量", "采购金额", "定点/支援/其它", "扶贫对象", "采购渠道", "备注", "提交时间", "联系方式", "采购方", "供应方"};
				filename = "caigou" + sdf.format(new Date()) + ".xls";
				content = new String[list1.size() + 1][19];
				for (int i = 0; i < list1.size(); i++) {
					content[i][0] = String.valueOf(i + 1);
					content[i][1] = list1.get(i).getUsername();
					content[i][2] = list1.get(i).getFirstname();
					content[i][3] = list1.get(i).getSecondname();
					content[i][4] = list1.get(i).getUnitname();
					content[i][5] = list1.get(i).getDepartment();
					content[i][6] = list1.get(i).getTotal();
					content[i][7] = list1.get(i).getGood();
					content[i][8] = list1.get(i).getContent();
					content[i][9] = String.valueOf(list1.get(i).getNumber());
					content[i][10] = String.valueOf(list1.get(i).getMoney());
					content[i][11] = list1.get(i).getCounty();
					content[i][12] = list1.get(i).getMode_id();
					content[i][13] = list1.get(i).getChannel();
					content[i][14] = list1.get(i).getRemarks();
					content[i][15] = list1.get(i).getTime();
					content[i][16] = String.valueOf(list1.get(i).getPhone());
					content[i][17] = list1.get(i).getPurchaser();
					content[i][18] = list1.get(i).getSupporter();
					singlesum += list1.get(i).getMoney();
				}
				content[list1.size()][0] = "帮销总计金额";
				content[list1.size()][1] = String.format("%.2f", singlesum);
				break;
		}
		return ExcelUtil.export(sheetName,title,content,filename);
	}

}
