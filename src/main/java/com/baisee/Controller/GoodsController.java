package com.baisee.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baisee.Entity.Goods;
import com.baisee.Service.GoodsService;

/**
 * 采购类型
 * @author Administrator
 *
 */
@Controller
@RequestMapping("Goods")
public class GoodsController {
	private final GoodsService service;

	public GoodsController(GoodsService service) {
		this.service = service;
	}

	@RequestMapping("findGoods")
	@ResponseBody
	public List<Goods> findGoods(HttpServletRequest req){
		String parameter = req.getParameter("list");
		System.out.println(parameter);
		return service.findGoods();
	}
}
