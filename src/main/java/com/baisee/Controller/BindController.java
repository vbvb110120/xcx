package com.baisee.Controller;

import com.baisee.Utils.Session_idUtil;
import com.baisee.redis.RedisUtilsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("bind")
public class BindController {
    RedisUtilsService redis;
    static ThreadLocal<String> localVar = new ThreadLocal<>();
    @RequestMapping("user")
    public String SelectByName(){
        return localVar.get();
    }

    @RequestMapping(value = "/WxLogin")
    public void login() {
        String skey=Session_idUtil.session_id();
        String st=new Date().toString();
        redis.set(skey, st);
    }
}
