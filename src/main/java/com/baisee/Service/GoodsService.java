package com.baisee.Service;

import java.util.List;

import org.apache.poi.ss.formula.functions.T;

import com.baisee.Entity.Channel;
import com.baisee.Entity.Goods;

public interface GoodsService {

	List<Goods> findGoods();

}
