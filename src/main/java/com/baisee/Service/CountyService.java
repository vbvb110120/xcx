package com.baisee.Service;

import java.util.List;

import com.baisee.Entity.County;
import com.baisee.Entity.CountyName;
import org.springframework.data.repository.query.Param;

public interface CountyService {

	List<County> findCounty();

	List<County> findByid(String id);

	List<CountyName> findByname(String name);
	List<CountyName> findByname1(@Param("name") String name);

}
