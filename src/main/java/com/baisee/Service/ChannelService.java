package com.baisee.Service;

import java.util.List;

import com.baisee.Entity.Channel;

public interface ChannelService {

	List<Channel> findChannel();

}
