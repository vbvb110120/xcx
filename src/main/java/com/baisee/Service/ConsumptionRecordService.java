package com.baisee.Service;

import com.baisee.Entity.Consumption;

import java.util.List;

public interface ConsumptionRecordService {
    List<Consumption> findRecordList();
}
