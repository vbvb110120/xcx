package com.baisee.Service;

import com.baisee.Entity.WxUser;

public interface LoginService {

	Integer findUserByOpenId(String open_id);

	void insertUser(WxUser user);

	void updateLogintime(WxUser user);

}
