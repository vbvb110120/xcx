package com.baisee.Service;

import com.baisee.Entity.Purchase;

import java.util.List;

public interface PurchaseRecordService {
    List<Purchase> findRecordList();

    List<Purchase> findRecordByName(String username,String unitname,String phone);

    void insertRecord(Purchase purchase);

    void deleteRecord(Integer id);

    void updateByName(String username,String unitname);

    void updateById(Purchase purchase);

    List<Purchase> findRecordByTotal(String username, String unitname, String department);

//    List<Purchase> findRecordByDepartment(String username, String unitname);

    List<Purchase> findRecordByDepartment(String unitname);

    List<Purchase> selectNoDepartmentOrNoTotal(String branch, String name, String phone);
}
