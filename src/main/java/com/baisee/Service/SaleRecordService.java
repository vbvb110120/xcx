package com.baisee.Service;

import com.baisee.Entity.Sale;

import java.util.List;

public interface SaleRecordService {
    List<Sale> findRecordList();

    void insertRecord(Sale sale);

    List<Sale> findRecordByName(String name, String username, String phone);

    void deleteRecord(Integer id);

    void updateByName(String username,String phone);

    void updateById(Sale sale);
}
