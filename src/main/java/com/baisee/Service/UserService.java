package com.baisee.Service;

import com.baisee.Entity.User;

public interface UserService {

	String findByName(String name);

}
