package com.baisee.Service;

import com.baisee.Entity.XcxUser;

import java.util.List;

public interface XcxUserService {
    public void insertRecord(XcxUser user);
    public List<XcxUser> selectByOpenid(String openId, int i);

    void deleteRecord(String username, String unitname);

    void updateRecord(XcxUser user);

    void updateRecord1(String username, String unitname);

    List<XcxUser> selectNoDepartmentOrNoTotal();

    void updateById(XcxUser user);
}
