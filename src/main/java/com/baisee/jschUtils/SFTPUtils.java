package com.baisee.jschUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SFTPUtils {
	private static final Logger LOG = LoggerFactory.getLogger(SFTPUtils.class);
	/**
	 * 连接ftp/sftp服务器
	 */
	public static void getConnect(SFTP s) throws JSchException {
        //      String privateKey ="key";
//        /** 密钥文件路径  */ 
//      String passphrase ="path";
        String host = "192.168.43.179";
		int port = 21;
		String username = "wyh";
		String password = "123456";

		Session session;
		Channel channel;
		ChannelSftp sftp;// sftp操作类
		JSch jsch = new JSch();
		// 设置密钥和密码
		// 支持密钥的方式登陆，只需在jsch.getSession之前设置一下密钥的相关信息就可以了
//      if (privateKey != null && !"".equals(privateKey)) {  
//             if (passphrase != null && "".equals(passphrase)) {  
//              //设置带口令的密钥  
//                 jsch.addIdentity(privateKey, passphrase);  
//             } else {  
//              //设置不带口令的密钥  
//                 jsch.addIdentity(privateKey);  
//             }  
//      }  
		session = jsch.getSession(username, host, port);
		session.setPassword(password);
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no"); // 不验证HostKey
		session.setConfig(config);
		session.connect(10000);
		try {
			session.connect();
			System.out.println("连接成功");
		} catch (Exception e) {
			if (session.isConnected())
				session.disconnect();
			LOG.error("连接服务器失败,请检查主机[" + host + "],端口[" + port + "],用户名[" + username + "],端口[" + port
			+ "]是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
			e.printStackTrace();
		}
		channel = session.openChannel("sftp");
		try {
			channel.connect();
		} catch (Exception e) {
			if (channel.isConnected())
				channel.disconnect();
			LOG.error("连接服务器失败,请检查主机[" + host + "],端口[" + port + "],用户名[" + username
					+ "],密码是否正确,以上信息正确的情况下请检查网络连接是否正常或者请求被防火墙拒绝.");
		}
		sftp = (ChannelSftp) channel;

		s.setChannel(channel);
		s.setSession(session);
		s.setSftp(sftp);

	}

	/**
	 * 断开连接
	 * 
	 */
	public static void disConn(Session session, Channel channel, ChannelSftp sftp) {
		if (null != sftp) {
			sftp.disconnect();
			sftp.exit();
		}
		if (null != channel) {
			channel.disconnect();
		}
		if (null != session) {
			session.disconnect();
		}
	}

	/**
	 * 上传文件
	 * 
	 * @param directory  上传的目录-相对于SFPT设置的用户访问目录， 为空则在SFTP设置的根目录进行创建文件（除设置了服务器全磁盘访问）
	 */
	public static void upload(String directory, MultipartFile file) throws Exception {

		SFTP s = new SFTP();
		getConnect(s);// 建立连接
		Session session = s.getSession();
		Channel channel = s.getChannel();
		ChannelSftp sftp = s.getSftp();// sftp操作类
		try {
			try {
				sftp.cd(directory); // 进入目录
			} catch (SftpException sException) {
				if (ChannelSftp.SSH_FX_NO_SUCH_FILE == sException.id) { // 指定上传路径不存在
					sftp.mkdir(directory);// 创建目录
					sftp.cd(directory); // 进入目录
				}
			}

			InputStream in = file.getInputStream();
			sftp.put(in,file.getOriginalFilename());
			in.close();

		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		} finally {
			disConn(session, channel, sftp);
		}
	}
	/**
           * 下载文件
     * @param directory 下载目录 根据SFTP设置的根目录来进行传入
     * @param downloadFile  下载的文件 
     * @param saveFile  存在本地的路径 
     */
    public static void download(String directory, String downloadFile,String saveFile) throws Exception {
        SFTP s=new SFTP();
        getConnect(s);//建立连接
        Session session = s.getSession();   
        Channel channel = s.getChannel();   
        ChannelSftp sftp = s.getSftp();// sftp操作类  
        try {
            sftp.cd(directory); //进入目录
            OutputStream out=new FileOutputStream(new File(saveFile,downloadFile));

            sftp.get(downloadFile, out);

            out.flush();
            out.close();

        } catch (Exception e) {
            throw new Exception(e.getMessage(),e); 
        } finally {
            disConn(session,channel,sftp);
        }
    }
    /**
     * 删除文件
     * @param directory 要删除文件所在目录 
     * @param deleteFile 要删除的文件
     */
    public static void delete(String directory, String deleteFile) throws Exception {
        SFTP s=new SFTP();
        getConnect(s);//建立连接
        Session session = s.getSession();   
        Channel channel = s.getChannel();   
        ChannelSftp sftp = s.getSftp();// sftp操作类  
        try {
            sftp.cd(directory); //进入的目录应该是要删除的目录的上一级
            sftp.rm(deleteFile);//删除目录
        } catch (Exception e) {
            throw new Exception(e.getMessage(),e); 
        } finally {
            disConn(session,channel,sftp);
        }
    }
    /** 
     * 列出目录下的文件 
     * @param directory  要列出的目录            
     * @return list 文件名列表 
     */
     public static List<String> listFiles(String directory) throws Exception { 
         SFTP s=new SFTP();
         getConnect(s);//建立连接
         Session session = s.getSession();   
         Channel channel = s.getChannel();   
         ChannelSftp sftp = s.getSftp();// sftp操作类  
         Vector fileList;
         List<String> fileNameList = new ArrayList<>();
         fileList = sftp.ls(directory); //返回目录下所有文件名称
         disConn(session,channel,sftp);

         for (Object o : fileList) {

             String fileName = ((LsEntry) o).getFilename();
             if (".".equals(fileName) || "..".equals(fileName)) {
                 continue;
             }
             fileNameList.add(fileName);

         } 

         return fileNameList; 
     }
     /**
      * 删除目录下所有文件
      * @param directory 要删除文件所在目录 
      */
     public static void deleteAllFile(String directory) throws Exception{
         SFTP s=new SFTP();
         getConnect(s);//建立连接
         Session session = s.getSession();   
         Channel channel = s.getChannel();   
         ChannelSftp sftp = s.getSftp();// sftp操作类     
         try {
             List <String> files=listFiles(directory);//返回目录下所有文件名称
             sftp.cd(directory); //进入目录

             for (String deleteFile : files) {
                 sftp.rm(deleteFile);//循环一次删除目录下的文件
             }
         } catch (Exception e) {
             throw new Exception(e.getMessage(),e); 
         } finally {
             disConn(session,channel,sftp);
         }

     }
     /**
      * 删除目录 (删除的目录必须为空)
      * @param deleteDir 要删除的目录 
      */
     public static void deleteDir(String deleteDir) throws Exception {
         SFTP s=new SFTP();
         getConnect(s);//建立连接
         Session session = s.getSession();   
         Channel channel = s.getChannel();   
         ChannelSftp sftp = s.getSftp();// sftp操作类   
         try {

             sftp.rmdir(deleteDir);

         } catch (Exception e) {
             throw new Exception(e.getMessage(),e); 
         } finally {
             disConn(session,channel,sftp);
         }
     }
     /**
      * 创建目录 
      * @param directory 要创建的目录 位置
      * @param dir 要创建的目录 
      */
        public static void creatDir(String directory,String dir) throws Exception {
            SFTP s=new SFTP();
            getConnect(s);//建立连接
            Session session = s.getSession();   
            Channel channel = s.getChannel();   
            ChannelSftp sftp = s.getSftp();// sftp操作类  
            try {
                sftp.cd(directory); 
                sftp.mkdir(dir);
            } catch (Exception e) {
                throw new Exception(e.getMessage(),e); 
            } finally {
                disConn(session,channel,sftp);
            }
       }
        /** 
         * 更改文件名 
         * @param directory  文件所在目录 
         * @param oldFileNm  原文件名 
         * @param newFileNm 新文件名       
         */
         public static void rename(String directory, String oldFileNm, String newFileNm) throws Exception { 
             SFTP s=new SFTP();
             getConnect(s);//建立连接
             Session session = s.getSession();   
             Channel channel = s.getChannel();   
             ChannelSftp sftp = s.getSftp();// sftp操作类    
             try {
                 sftp.cd(directory); 
                 sftp.rename(oldFileNm, newFileNm); 
             } catch (Exception e) {
                 throw new Exception(e.getMessage(),e); 
             } finally {
                 disConn(session,channel,sftp);
             }
         }
         /**
          * 进入目录
          */
         public static void cd(String directory)throws Exception { 

             SFTP s=new SFTP();
             getConnect(s);//建立连接
             Session session = s.getSession();   
             Channel channel = s.getChannel();   
             ChannelSftp sftp = s.getSftp();// sftp操作类   
             try {
                 sftp.cd(directory); //目录要一级一级进
             } catch (Exception e) {
                 throw new Exception(e.getMessage(),e); 
             } finally {
                 disConn(session,channel,sftp);
             }
         }
    
}
