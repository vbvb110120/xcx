package com.baisee.jschUtils;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
public class FTPUtils {
	//用于打印日志
    private static final Logger log = Logger.getLogger(FTPUtils.class);

    //设置私有不能实例化
    private FTPUtils() {

    }
   
    /**
     * 上传
     *
     * @param inputStream 要上传文件的输入流
     * @param saveName    设置上传之后的文件名
     */
    public static boolean upload(InputStream inputStream, String saveName) {
        String hostname =FTP.getHostname();
        int port =FTP.getPort();
        String username=FTP.getUsername();
        String password=FTP.getPassword();
        String workingPath=FTP.getWorkingpath();
    	boolean flag = false;
        FTPClient ftpClient = new FTPClient();
        //1 测试连接
        if (connect(ftpClient, hostname, port, username, password)) {
            try {
                //2 检查工作目录是否存在
//                String pwd = ftpClient.printWorkingDirectory();
//                System.out.println(pwd);
//                FTPFile[] ftpFiles = null;
//                try {
//                    ftpFiles = ftpClient.listFiles(pwd);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                for (int i = 0; ftpFiles != null && i < ftpFiles.length; i++) {
//                    FTPFile file = ftpFiles[i];
//                }
                if (ftpClient.changeWorkingDirectory(workingPath)) {
                    // 3 检查是否上传成功
                   if (storeFile(ftpClient, saveName, inputStream)) {
                        flag = true;
                        disconnect(ftpClient);
                    }
                }
            } catch (IOException e) {
                log.error("工作目录不存在");
                e.printStackTrace();
                disconnect(ftpClient);
            }
        }
        return flag;
    }

    /**
     * 断开连接
     */
    public static void disconnect(FTPClient ftpClient) {
        if (ftpClient.isConnected()) {
            try {
                ftpClient.disconnect();
                log.error("已关闭连接");
            } catch (IOException e) {
                log.error("没有关闭连接");
                e.printStackTrace();
            }
        }
    }

    /**
     * 测试是否能连接
     ** @param hostname  ip或域名地址
     * @param port      端口
     * @param username  用户名
     * @param password  密码
     * @return 返回真则能连接
     */
    public static boolean connect(FTPClient ftpClient, String hostname, int port, String username, String password) {
        boolean flag = false;
        try {
            //ftp初始化的一些参数
            ftpClient.connect(hostname, port);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.setControlEncoding("UTF-8");
            System.err.println("OK");
            if (ftpClient.login(username, password)) {
                log.info("连接ftp成功");
                flag = true;
            } else {
                log.error("连接ftp失败，可能用户名或密码错误");
                try {
                    disconnect(ftpClient);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            log.error("连接失败，可能ip或端口错误");
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 上传文件
     ** @param saveName        全路径。如/home/public/a.txt
     * @param fileInputStream 要上传的文件流
     */
    public static boolean storeFile(FTPClient ftpClient, String saveName, InputStream fileInputStream) {
        boolean flag = false;
        try {
            if (ftpClient.storeFile(saveName, fileInputStream)) {
                Thread.sleep(1000);
                flag = true;
                log.error("上传成功");
                disconnect(ftpClient);
            }
        } catch (IOException | InterruptedException e) {
            log.error("上传失败");
            disconnect(ftpClient);
            e.printStackTrace();
        }
        return flag;
    }
}
