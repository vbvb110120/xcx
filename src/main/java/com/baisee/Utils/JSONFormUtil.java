package com.baisee.Utils;

import com.alibaba.fastjson.JSONArray;
/**
 * JSON字符串的处理
 * @author wyh
 *
 */
public class JSONFormUtil {
	/**
	 * JSON转对象
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T JSONtoObject(String json,Class<T> clazz){
		T t = JSONArray.parseObject(json, clazz);
		return t;
	}
}
