package com.baisee.Utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.codec.binary.Base64;

/**
 * 加密算法。 <br/>
 * 1.MD5 <br/>
 * 2.SHA-256 <br/>
 * 3.对称加解密算法。
 */
public class EncryptUtil {

	/**
	 * 使用MD5加密
	 */
	@SuppressWarnings("unused")
	public static String encryptMd5(String inStr) throws Exception {

		MessageDigest md;
		String out = null;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(inStr.getBytes());
			return new String(Base64.encodeBase64(digest));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * 输出明文按sha-256加密后的密文
	 * 
	 * @param inputStr
	 *            明文
	 */
	public static synchronized String encryptSha256(String inputStr) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] digest = md.digest(inputStr.getBytes(StandardCharsets.UTF_8));
			return new String(Base64.encodeBase64(digest));
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static String byte2hex(byte[] b) {

		StringBuilder hs = new StringBuilder();
		String stmp;

		for (byte value : b) {
			stmp = (Integer.toHexString(value & 0XFF));
			if (stmp.length() == 1) {
				hs.append("0").append(stmp);
			} else {
				hs.append(stmp);
			}
			// if(16==hs.length()) break;
		}
		return hs.toString().toLowerCase();
	}

	/**
	 * 密钥
	 */
	private static final String key = "@#$%^6a7";

	/**
	 * 对称解密算法

	 */
	public static String decrypt(String message) throws Exception {
		byte[] bytesrc = stringToBytes(message);
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(StandardCharsets.UTF_8));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
		IvParameterSpec iv = new IvParameterSpec(key.getBytes(StandardCharsets.UTF_8));

		cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);

		byte[] retByte = cipher.doFinal(bytesrc);
		return new String(retByte, StandardCharsets.UTF_8);
	}

	/**
	 * 对称加密算法
	 */
	public static String encrypt(String message) throws Exception {
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");

		DESKeySpec desKeySpec = new DESKeySpec(key.getBytes(StandardCharsets.UTF_8));

		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		SecretKey secretKey = keyFactory.generateSecret(desKeySpec);
		IvParameterSpec iv = new IvParameterSpec(key.getBytes(StandardCharsets.UTF_8));
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

		return bytesToString(cipher.doFinal(message.getBytes(StandardCharsets.UTF_8)));
	}

	/**
	 * String转Byte数组
	 */
	private static byte[] stringToBytes(String temp) {
		byte[] digest = new byte[temp.length() / 2];
		for (int i = 0; i < digest.length; i++) {
			String byteString = temp.substring(2 * i, 2 * i + 2);
			int byteValue = Integer.parseInt(byteString, 16);
			digest[i] = (byte) byteValue;
		}

		return digest;
	}

	/**
	 * Byte数组转String
	 */
	private static String bytesToString(byte[] b) {
		StringBuilder hexString = new StringBuilder();
		for (byte value : b) {
			String plainText = Integer.toHexString(0xff & value);
			if (plainText.length() < 2)
				plainText = "0" + plainText;
			hexString.append(plainText);
		}

		return hexString.toString();
	}

	public static void main(String[] args) {
		System.out.print(EncryptUtil.encryptSha256("123456"));
	}

}
