package com.baisee.Utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
//import javax.crypto.KeyGenerator;
//import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;

@Slf4j
public class AESUtils {

    public static String CBC_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
//    public static String ECB_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String KEY_ALGORITHM = "AES";
//    private static final String SECURERANDOM_MODE = "SHA1PRNG";


    /**
     * 带有初始变量的解密（微信用）
     *
     * @param content     密文
     * @param skey        密钥
     * @param ivParameter 初始向量
     */
    public static String weixinDecrypt(String content, String skey, String ivParameter) {
        try {
            byte[] keyByte = Base64.decodeBase64(skey);
            byte[] contentByte = Base64.decodeBase64(content);
            byte[] ivByte = Base64.decodeBase64(ivParameter);
            // 生成密码
            SecretKeySpec keySpec = new SecretKeySpec(keyByte, KEY_ALGORITHM);
            // 生成IvParameterSpec
            IvParameterSpec iv = new IvParameterSpec(ivByte);
            // 初始化解密 指定模式 AES/CBC/PKCS5Padding
            Cipher cipher = Cipher.getInstance(CBC_CIPHER_ALGORITHM);
            // 指定解密模式 传入密码 iv
            cipher.init(Cipher.DECRYPT_MODE, keySpec, iv);
            // 解密
            byte[] result = cipher.doFinal(contentByte);
            return new String(result, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            //log.error("【解密错误】{}", ex.getMessage());
            return null;
        }
    }

//    /**
//     * AES 加密操作
//     *
//     * @param content 待加密内容
//     * @param key     加密密钥
//     * @return 返回Base64转码后的加密数据
//     */
//    public static String encrypt(String content, String key) {
//        try {
//            // 密文转换 byte[]
//            byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
//            // 创建密码器
//            Cipher cipher = Cipher.getInstance(ECB_CIPHER_ALGORITHM);
//            // 初始化为加密模式 并传入密码 密码器
//            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(key));
//            // 加密
//            byte[] result = cipher.doFinal(byteContent);
//            //通过Base64转码返回
//            return new BASE64Encoder().encode(result);
//        } catch (Exception ex) {
//            //log.error("【加密错误】{}", ex.getMessage());
//            return null;
//        }
//    }
//
//    /**
//     * AES 解密操作
//     */
//    public static String decrypt(String content, String key) {
//        try {
//            // 创建密码器
//            Cipher cipher = Cipher.getInstance(ECB_CIPHER_ALGORITHM);
//            // 初始化为解密模式 并传入密码 密码器
//            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(key));
//            // 执行解密操作
//            byte[] result = cipher.doFinal(new BASE64Decoder().decodeBuffer(content));
//            // 转换成 string
//            return new String(result, StandardCharsets.UTF_8);
//        } catch (Exception ex) {
//            //log.error("【解密错误】{}", ex.getMessage());
//            return null;
//        }
//    }

//    /**
//     * 生成加密秘钥
//
//     */
//    private static SecretKeySpec getSecretKey(final String password) throws NoSuchAlgorithmException {
//        //生成指定算法密钥的生成器
//        KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM);
//        //================重点========================
//        // 指定随机数算法 防止win linux 生成的随机数不一致
//        SecureRandom secureRandom = SecureRandom.getInstance(SECURERANDOM_MODE);
//        secureRandom.setSeed(password.getBytes());
//        keyGenerator.init(128, secureRandom);
//        //===============重点=========================
//        //生成密钥
//        SecretKey secretKey = keyGenerator.generateKey();
//        //转换成AES的密钥
//        return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);
//    }

}