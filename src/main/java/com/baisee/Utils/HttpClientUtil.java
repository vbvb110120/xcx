package com.baisee.Utils;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import net.sf.json.JSONObject;
/**
 * 发起网络请求
 * @author wyh
 *
 */
public class HttpClientUtil {
	/**
	 * 发起post请求返回map
	 */
	public static Map sendHttpClient(StringBuilder url) throws ParseException, IOException {
			Map<String, Object> map;
			HttpClient client = HttpClientBuilder.create().build();// 构建一个Client
			HttpPost post = new HttpPost(url.toString()); // 构建一个Post请求
			HttpResponse response = client.execute(post);// 提交POST请求
			HttpEntity result = response.getEntity();// 拿到返回的HttpResponse的"实体"
			String content = EntityUtils.toString(result);
			System.out.println(content);// 打印返回的信息
			JSONObject res = JSONObject.fromObject(content);// 把信息封装为json
			// 把信息封装到map
			map = MdzwUtils.parseJSON2Map(res);// json转map工具
		
		return map;
	}
}
