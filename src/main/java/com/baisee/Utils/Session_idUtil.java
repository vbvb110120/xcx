package com.baisee.Utils;

import java.util.UUID;

/**
 * 生成自定义登录态
 * @author wyh
 *
 */
public class Session_idUtil {
	/**
	 * 生成随机数
	 * @return
	 */
	public static String session_id(){
		return UUID.randomUUID().toString();
	}
}
